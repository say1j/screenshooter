﻿using System;
using Emgu.CV;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading.Tasks;
using Emgu.CV.Structure;
using FastBitmapLib;
using System.IO;
using OpenTK.Platform.Windows;
using OpenTK.Graphics.ES11;

namespace ScreenshoterLinuxFramework
{
    public class Main
    {
        public static Bitmap GenerateBitmap(string _float, string _patern, string _paintindex, string _type, string _quality, string _link, string _assetid, string _itemname, string _is_stattrak)
        {
            Bitmap DownloadedImage = RequestWeb.DownloadImageFromUrl(_link);
            return MainUsage(DownloadedImage, _float, _patern, _paintindex, _type, _quality, _assetid, _itemname, _is_stattrak);
        }

        public static Bitmap MainUsage(Bitmap DownloadedImage, string _float, string _patern, string _paintindex, string _type, string _quality, string _assetid, string _itemname, string _is_stattrak)
        {
            var image_screen = ConvertTo32bpp(DownloadedImage);
            //Mat img = Emgu.CV.CvInvoke.Imread("ZUW9YIJ_image.jpg", Emgu.CV.CvEnum.LoadImageType.AnyColor);
            string current_path = "";
            if (image_screen.Width > 3000)
                current_path = "img/background_screenshot4k.jpg";
            else if (image_screen.Width > 2500)
                current_path = "img/background_screenshot_qhd.jpg";
            else
                current_path = "img/background_hd.jpg";
            var imgback = (Bitmap)Image.FromFile(current_path);
            imgback = ConvertTo32bpp(imgback);
            image_screen.Save("out.jpg", ImageFormat.Jpeg);
            Image<Rgb, Byte> imgfront = new Image<Rgb, Byte>(image_screen);
            Image<Rgb, Byte> imgbacks = new Image<Rgb, Byte>(imgback);
            Image<Rgb, Byte> Difference;
            File.AppendAllText("ttt.txt", "Difference" + Environment.NewLine);
            Difference = imgbacks.AbsDiff(imgfront);
            /*
            var image_screen = ConvertTo32bpp(DownloadedImage);
            //Color asd = image_screen.GetPixel(600, 500);
            Image<Bgr, Byte> imageCV = new Image<Bgr, byte>(image_screen); //Image Class from Emgu.CV
            Mat img = imageCV.Mat; //This is your Image converted to Mat
            string current_path = "";
            if (DownloadedImage.Width > 2000)
                current_path = "img/background_screenshot4k.jpg";
            else
                current_path = "img/background_hd.jpg";

            Mat imgback = Emgu.CV.CvInvoke.Imread(current_path, Emgu.CV.CvEnum.LoadImageType.AnyColor);
            Image<Bgr, Byte > imgeOrigenal = img.ToImage<Bgr, Byte>();
            Image<Bgr, Byte> imgeback = imgback.ToImage<Bgr, Byte>();
            Image<Bgr, Byte> Difference;
            Difference = imgeback.AbsDiff(imgeOrigenal);*/
            // var temp = imgeOrigenal.Cmp(imgeback, Emgu.CV.CvEnum.CmpType.GreaterEqual);
            Difference = Difference.Erode(1);
            // Difference.ToBitmap().Save("output1.jpg");
            Color clr = Difference.ToBitmap().GetPixel(1, 1);
            CvInvoke.MedianBlur(Difference, Difference, 9);

            //CvInvoke.GaussianBlur(Difference, Difference, Difference.Size, 0);
            using (var fImage_background = ConvertTo32bpp(Difference.ToBitmap()).FastLock())
            {
                using (var fImage_screen = image_screen.FastLock())
                {
                    Parallel.For(0, Difference.Width, i =>
                    {
                        Parallel.For(0, Difference.Height, j =>
                        {
                            if (fImage_background.GetPixel(i, j).ToArgb() + 35 > clr.ToArgb() - 35 && fImage_background.GetPixel(i, j).ToArgb() - 35 < clr.ToArgb() + 35)
                            {
                                fImage_screen.SetPixel(i, j, Color.Turquoise);
                            }
                        });
                    });/*
                    Parallel.For(0, 4096, i =>
                    {
                        Parallel.For(2682, 2906, j =>
                        {
                            fImage_screen.SetPixel(i, j, Color.Empty);
                        });
                    });*/
                }
            }
            string qr_link = "";
            if (String.Equals(_is_stattrak.ToLower(), "false"))
                qr_link = $"https://waxpeer.com/{_itemname.Replace("| ", "").Replace(" ", "-").Replace("(", "").Replace(")", "").ToLower()}/item/{_assetid}";
            else
                qr_link = $"https://waxpeer.com/{"stattrak-" + _itemname.Replace(" |", "").Replace(" ", "-").Replace("(", "").Replace(")", "").ToLower()}/item/{_assetid}";
            if (image_screen.Width > 3000)
            {
                return ImageFon.CreateFinalImg4K(image_screen, qr_link, _patern, Convert.ToDouble(_float), _paintindex, _quality, _type, _itemname, _is_stattrak);
            }
            else if (image_screen.Width > 2500)
            {
                return ImageFon.CreateFinalImgQHD(image_screen, qr_link, _patern, Convert.ToDouble(_float), _paintindex, _quality, _type, _itemname, _is_stattrak);
            }
            else
            {
                return ImageFon.CreateFinalImg(image_screen, qr_link, _patern, Convert.ToDouble(_float), _paintindex, _quality, _type, _itemname, _is_stattrak);
            }
        }


        public static byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, ImageFormat.Jpeg);
                return ms.ToArray();
            }
        }
        public static Bitmap ConvertTo32bpp(Image img)
        {
            var bmp = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format32bppRgb);
            using (var gr = Graphics.FromImage(bmp))
                gr.DrawImage(img, new Rectangle(0, 0, img.Width, img.Height));
            return bmp;
        }
        public static Bitmap ConvertTo24bpp(Image img)
        {
            var bmp = new Bitmap(img.Width, img.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            using (var gr = Graphics.FromImage(bmp))
                gr.DrawImage(img, new Rectangle(0, 0, img.Width, img.Height));
            return bmp;
        }
    }
}
