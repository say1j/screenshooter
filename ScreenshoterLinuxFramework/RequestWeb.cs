﻿using System;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace ScreenshoterLinuxFramework
{
    public class RequestWeb
    {
        public static string GetItemInfo(string url)
        {
            string method = "GET";
            NameValueCollection headers = null;
            CookieContainer cookies = new CookieContainer();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = method;
            request.Accept = "text/javascript, text/html, application/xml, text/xml, */*";
            request.UserAgent = "Mozilla/5.0 (Linux; U; Android 4.1.1; en-us; Google Nexus 4 - 4.1.1 - API 16 - 768x1280 Build/JRO03S) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30";
            request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            try
            {
                request.ServicePoint.ConnectionLimit = 500;
                request.Timeout = 20000;
                request.ServicePoint.Expect100Continue = false;
                request.KeepAlive = true;
            }
            catch (Exception) { }

            if (headers != null)
            {
                request.Headers.Add(headers);
            }

            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        HandleFailedWebRequestResponse(response, url);
                        return null;
                    }

                    using (StreamReader responseStream = new StreamReader(response.GetResponseStream()))
                    {
                        string responseData = responseStream.ReadToEnd();
                        return responseData;
                    }
                }
            }
            catch (WebException e)
            {
                HandleFailedWebRequestResponse(e.Response as HttpWebResponse, url);
                return null;
            };
        }
        /*
        public static Bitmap DownloadImageFromUrl(string imageUrl)
        {
            try
            {
                System.Net.WebRequest request = System.Net.WebRequest.Create(imageUrl);
                System.Net.WebResponse response = request.GetResponse();
                System.IO.Stream responseStream =
                    response.GetResponseStream();
                Bitmap bitmap2 = new Bitmap(responseStream, true);
                return bitmap2;
            }
            catch (Exception ex)
            {
                return null;
            }
        }*/

        public static Bitmap DownloadImageFromUrl(string imageUrl)
        {
            HttpWebRequest wreq;
            HttpWebResponse wresp;
            Stream mystream;
            Bitmap bmp;

            bmp = null;
            mystream = null;
            wresp = null;
            try
            {
                wreq = (HttpWebRequest)WebRequest.Create(imageUrl);
                wreq.AllowWriteStreamBuffering = true;

                wresp = (HttpWebResponse)wreq.GetResponse();

                if ((mystream = wresp.GetResponseStream()) != null)
                    bmp = new Bitmap(mystream);
            }
            finally
            {
                if (mystream != null)
                    mystream.Close();

                if (wresp != null)
                    wresp.Close();
            }
            return (bmp);
        }

        private static void HandleFailedWebRequestResponse(HttpWebResponse response, string requestURL)
        {
            if (response == null) return;
        }
    }
}
