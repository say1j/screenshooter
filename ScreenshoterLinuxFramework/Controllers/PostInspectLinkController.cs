﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace ScreenshoterLinuxFramework.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostInspectLinkController : ControllerBase
    {
        [HttpGet]
        public async Task<ActionResult<string>> Get(string url)
        {
            try
            {
                if (string.IsNullOrEmpty(url))
                {
                    return Ok(new { success = false, msg = "Missed value in query" });
                }
                else
                {
                    var url_csmoney = "https://3d.cs.money/addNewItemByInpectLink?inspectLink=" + url;
                    string money_answer = RequestWeb.GetItemInfo(url_csmoney);
                    JObject money_json = JObject.Parse(money_answer);
                    string _float = money_json["item"]["floatvalue"].ToString();
                    string _patern = money_json["item"]["paintseed"].ToString();
                    string _paintindex = money_json["item"]["paintindex"].ToString();
                    string _type = money_json["item"]["item_type"].ToString();
                    string _quality = money_json["item"]["wear_name"].ToString();
                    string _link = money_json["link"].ToString();
                    string _assetid = money_json["item"]["assetid"].ToString();
                    string _itemname = money_json["item"]["item_name"].ToString() + " | " + money_json["item"]["skin_name"].ToString() + " (" + money_json["item"]["wear_name"].ToString() + ")";
                    string _is_stattrak = money_json["item"]["is_stattrak"].ToString();

                    Bitmap output_bitmap = Main.GenerateBitmap(_float, _patern, _paintindex, _type, _quality, _link, _assetid, _itemname, _is_stattrak);
                    Byte[] data;
                    using (var memoryStream = new MemoryStream())
                    {
                        output_bitmap.Save(memoryStream, ImageFormat.Jpeg);

                        data = memoryStream.ToArray();
                    }
                    return File(data, "image/jpeg");
                }
            }
            catch (Exception ex)
            {
                return Ok(new { success = false, msg = ex.Message + Environment.NewLine + ex.ToString() });
            }
        }
        
    }
}