﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Threading.Tasks;
using ZXing;
using ZXing.QrCode.Internal;
using ZXing.Rendering;

namespace ScreenshoterLinuxFramework
{
    public class ImageFon
    {
        public static Bitmap CreateFinalImg(Bitmap img1, string url, string pattern, double item_float, string paint_index, string quality, string type, string name, string _is_stattrak)
        {
            var fon = (Bitmap)Image.FromFile("img/_1.jpg");
            var fon2 = (Bitmap)Image.FromFile("img/_2.jpg");
            fon.SetResolution(96.0F, 96.0F);
            var info = (Bitmap)Image.FromFile("img/Баннер 2 (2).jpg");
            info.SetResolution(96.0F, 96.0F);
            Color clr = img1.GetPixel(1, 1);
            img1.MakeTransparent(Color.Turquoise);
            Graphics g = Graphics.FromImage(fon);
            g.DrawImage(img1, 80, 0, Convert.ToSingle(img1.Width * 0.87), Convert.ToSingle(img1.Height * 0.87));
            fon2.SetResolution(96.0F, 96.0F);
            Color clr2 = img1.GetPixel(1, 1);
            img1.MakeTransparent(Color.Turquoise);
            Graphics g2 = Graphics.FromImage(fon2);
            g2.DrawImage(img1, 80, -1230, Convert.ToSingle(img1.Width * 0.87), Convert.ToSingle(img1.Height * 0.87));
            Bitmap res = new Bitmap(Math.Max(fon.Width, fon2.Width), Math.Max(fon.Height, fon2.Height) * 3 - 100);
            Graphics g1 = Graphics.FromImage(res);
            var codeImage = GenerateQR(340, 340, url, "img/logoqrhd.png");
            var rect = new System.Drawing.Rectangle(1565, 520, codeImage.Width, codeImage.Height);
            using (Graphics gr = Graphics.FromImage(info))
            {
                gr.DrawImage(codeImage, rect);
            }
            Graphics info_g = Graphics.FromImage(info);
            info_g.DrawString(pattern, new Font("Verdana", (float)35),
            new SolidBrush(Color.White), 400, 811); // РИСУЕМ ФЛОТ
            info_g.DrawString(paint_index, new Font("Verdana", (float)35),
            new SolidBrush(Color.White), 400, 625); // РИСУЕМ ПАТТЕРН
            info_g.DrawString(Math.Round(item_float, 9).ToString().Replace(",", "."), new Font("Verdana", (float)35),
            new SolidBrush(Color.White), 400, 434); // paint_index
            info_g.DrawString(quality, new Font("Verdana", (float)35),
            new SolidBrush(Color.White), 1120, 625); // качество
            info_g.DrawString(type, new Font("Verdana", (float)35),
            new SolidBrush(Color.White), 1120, 434); // type
            if(String.Equals(_is_stattrak.ToLower(), "true"))
            {
                info_g.DrawString("StatTrak™", new Font("Verdana", (float)35),
                new SolidBrush(Color.Orange), 72, 100); // Name
                info_g.DrawString(name, new Font("Verdana", (float)50),
                new SolidBrush(Color.Orange), 70, 155); // Name
            }
            else
            {
                info_g.DrawString(name, new Font("Verdana", (float)50),
                new SolidBrush(Color.White), 70, 125); // Name
            }
            g1.SmoothingMode = SmoothingMode.AntiAlias;
            g1.DrawImage(fon, 0, 0);
            g1.DrawImage(fon2, 0, fon.Height);
            g1.DrawImage(info, 0, fon.Height + fon2.Height);
            return res;
        }

        public static Bitmap CreateFinalImgQHD(Bitmap img1, string url, string pattern, double item_float, string paint_index, string quality, string type, string name, string _is_stattrak)
        {
            var fon = (Bitmap)Image.FromFile("img/_1_qhd.jpg");
            var fon2 = (Bitmap)Image.FromFile("img/_2_qhd.jpg");
            fon.SetResolution(96.0F, 96.0F);
            var info = (Bitmap)Image.FromFile("img/Баннер 2 qhd.jpg");
            info.SetResolution(96.0F, 96.0F);
            img1.SetResolution(96.0F, 96.0F);
            Color clr = img1.GetPixel(1, 1);
            img1.MakeTransparent(clr);
            Graphics g = Graphics.FromImage(fon);
            g.DrawImage(img1, 170, 0, Convert.ToSingle(img1.Width * 0.9), Convert.ToSingle(img1.Height * 0.9));
            fon2.SetResolution(96.0F, 96.0F);
            //var img_2 = tmp(img1, 0, 2910, 3840, 2160);
            Color clr2 = img1.GetPixel(1, 1);
            img1.MakeTransparent(clr);
            Graphics g2 = Graphics.FromImage(fon2);
            g2.DrawImage(img1, 170, -1700, Convert.ToSingle(img1.Width * 0.9), Convert.ToSingle(img1.Height * 0.9));
            Bitmap res = new Bitmap(Math.Max(fon.Width, fon2.Width), Math.Max(fon.Height, fon2.Height) * 3 - 170);
            Graphics g1 = Graphics.FromImage(res);
            var codeImage = GenerateQR(440, 440, url, "img/logoqrqhd.png");
            var rect = new System.Drawing.Rectangle(2020, 685, codeImage.Width, codeImage.Height);
            using (Graphics gr = Graphics.FromImage(info))
            {
                gr.DrawImage(codeImage, rect);
            }
            Graphics info_g = Graphics.FromImage(info);
            info_g.DrawString(pattern, new Font("Verdana", (float)50),
            new SolidBrush(Color.White), 560, 801); // РИСУЕМ ПАТТЕРН
            info_g.DrawString(Math.Round(item_float, 5).ToString().Replace(",", "."), new Font("Verdana", (float)50),
            new SolidBrush(Color.White), 560, 556); // РИСУЕМ ФЛОТ
            info_g.DrawString(paint_index, new Font("Verdana", (float)50),
            new SolidBrush(Color.White), 560, 1040); // paint_index
            info_g.DrawString(quality, new Font("Verdana", (float)50),
            new SolidBrush(Color.White), 1483, 801); // качество
            info_g.DrawString(type, new Font("Verdana", (float)50),
            new SolidBrush(Color.White), 1483, 556); // type
            if (String.Equals(_is_stattrak.ToLower(), "true"))
            {
                info_g.DrawString("StatTrak™", new Font("Verdana", (float)50),
                new SolidBrush(Color.Orange), 90, 120); // Name
                info_g.DrawString(name, new Font("Verdana", (float)65),
                new SolidBrush(Color.Orange), 90, 200); // Name
            }
            else
            {
                info_g.DrawString(name, new Font("Verdana", (float)70),
                new SolidBrush(Color.White), 90, 180); // Name
            }
            g1.SmoothingMode = SmoothingMode.AntiAlias;
            g1.DrawImage(fon, 0, 0);
            g1.DrawImage(fon2, 0, fon.Height);
            g1.DrawImage(info, 0, fon.Height + fon2.Height);
            return res;
        }

        public static Bitmap CreateFinalImg4K(Bitmap img1, string url, string pattern, double item_float, string paint_index, string quality, string type, string name, string _is_stattrak)
        {
            var fon = (Bitmap)Image.FromFile("img/_1_4k.jpg");
            var fon2 = (Bitmap)Image.FromFile("img/_2_4k.jpg");
            fon.SetResolution(96.0F, 96.0F);
            var info = (Bitmap)Image.FromFile("img/Баннер 2 4к.jpg");
            info.SetResolution(96.0F, 96.0F);
            img1.SetResolution(96.0F, 96.0F);
            Color clr = img1.GetPixel(1, 1);
            img1.MakeTransparent(clr);
            Graphics g = Graphics.FromImage(fon);
            g.DrawImage(img1, 150, 0, Convert.ToSingle(img1.Width * 0.85), Convert.ToSingle(img1.Height * 0.85));
            fon2.SetResolution(96.0F, 96.0F);
            //var img_2 = tmp(img1, 0, 2910, 3840, 2160);
            Color clr2 = img1.GetPixel(1, 1);
            img1.MakeTransparent(clr);
            Graphics g2 = Graphics.FromImage(fon2);
            g2.DrawImage(img1, 150, -2500, Convert.ToSingle(img1.Width * 0.85), Convert.ToSingle(img1.Height * 0.85));
            Bitmap res = new Bitmap(Math.Max(fon.Width, fon2.Width), Math.Max(fon.Height, fon2.Height) * 3 - 260);
            Graphics g1 = Graphics.FromImage(res);
            var codeImage = GenerateQR(640, 640, url, "img/logoqr.png");
            var rect = new System.Drawing.Rectangle(3035, 1040, codeImage.Width, codeImage.Height);
            using (Graphics gr = Graphics.FromImage(info))
            {
                gr.DrawImage(codeImage, rect);
            }
            Graphics info_g = Graphics.FromImage(info);
            info_g.DrawString(pattern, new Font("Verdana", (float)70),
            new SolidBrush(Color.White), 810, 1208); // РИСУЕМ ПАТТЕРН
            info_g.DrawString(Math.Round(item_float, 5).ToString().Replace(",", "."), new Font("Verdana", (float)70),
            new SolidBrush(Color.White), 810, 840); // РИСУЕМ ФЛОТ
            info_g.DrawString(paint_index, new Font("Verdana", (float)70),
            new SolidBrush(Color.White), 810, 1570); // paint_index
            info_g.DrawString(quality, new Font("Verdana", (float)70),
            new SolidBrush(Color.White), 2200, 1208); // качество
            info_g.DrawString(type, new Font("Verdana", (float)70),
            new SolidBrush(Color.White), 2200, 840); // type
            if (String.Equals(_is_stattrak.ToLower(), "true"))
            {
                info_g.DrawString("StatTrak™", new Font("Verdana", (float)65),
                new SolidBrush(Color.Orange), 130, 185); // Name
                info_g.DrawString(name, new Font("Verdana", (float)90),
                new SolidBrush(Color.Orange), 123, 295); // Name
            }
            else
            {
                info_g.DrawString(name, new Font("Verdana", (float)90),
                new SolidBrush(Color.White), 123, 285); // Name
            }
            g1.SmoothingMode = SmoothingMode.AntiAlias;
            g1.DrawImage(fon, 0, 0);
            g1.DrawImage(fon2, 0, fon.Height);
            g1.DrawImage(info, 0, fon.Height + fon2.Height);
            return res;
        }

        public static Bitmap GenerateQR(int width, int height, string text, string path)
        {
            var bw = new ZXing.BarcodeWriter();

            var encOptions = new ZXing.Common.EncodingOptions
            {
                Width = width,
                Height = height,
                Margin = 0,
                PureBarcode = false
            };

            encOptions.Hints.Add(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);

            bw.Renderer = new BitmapRenderer();
            bw.Options = encOptions;
            bw.Format = ZXing.BarcodeFormat.QR_CODE;
            Bitmap bm = bw.Write(text);
            Bitmap overlay = new Bitmap($"{path}");

            int deltaHeigth = bm.Height - overlay.Height;
            int deltaWidth = bm.Width - overlay.Width;

            Graphics g = Graphics.FromImage(bm);
            g.DrawImage(overlay, new Point(deltaWidth / 2, deltaHeigth / 2));

            return bm;
        }

        /*
        private static System.Drawing.Image GenerateQRCode(string content, int size)
        {
            QrEncoder encoder = new QrEncoder(ErrorCorrectionLevel.H);
            QrCode qrCode;
            encoder.TryEncode(content, out qrCode);

            GraphicsRenderer gRenderer = new GraphicsRenderer(new FixedModuleSize(4, QuietZoneModules.Two), System.Drawing.Brushes.Black, System.Drawing.Brushes.White);
            MemoryStream ms = new MemoryStream();
            gRenderer.WriteToStream(qrCode.Matrix, ImageFormat.Bmp, ms);

            var imageTemp = new Bitmap(ms);
            var image = new Bitmap(imageTemp, new System.Drawing.Size(new System.Drawing.Point(size, size)));

            return (System.Drawing.Image)image;
        }*/
    }
}
